package org.aossie.agoraandroid.createelection

import androidx.lifecycle.ViewModel

class CreateElectionOneViewModel : ViewModel() {
  var mElectionName: String? = null
  var mElectionDescription: String? = null
  var mStartDate: String? = null
  var mEndDate: String? = null

}
